build:
	python -m build

upload:
	python -m twine upload dist/*

clean:
	rm -rf dist/
	rm -rf showdialog.egg-info
	rm -rf showdialog/__pycache__/
	rm -rf .mypy_cache/

check:
	python -m mypy .
	python -m pylint ./showdialog
